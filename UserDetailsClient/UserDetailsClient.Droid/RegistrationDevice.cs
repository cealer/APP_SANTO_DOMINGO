﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using UserDetailsClient.Interfaces;
using Gcm.Client;
using Android.Util;

[assembly: Dependency(typeof(UserDetailsClient.Droid.RegistrationDevice))]
namespace UserDetailsClient.Droid
{
    public class RegistrationDevice : IRegisterDevice
    {
        #region Methods
        public void RegisterDevice()
        {
            var mainActivity = MainActivity.GetInstance();
            GcmClient.CheckDevice(mainActivity);
            GcmClient.CheckManifest(mainActivity);

            Log.Info("MainActivity", "Registering...");
            GcmClient.Register(mainActivity, UserDetailsClient.Droid.Constants.SenderID);
        }
        #endregion
    }

}