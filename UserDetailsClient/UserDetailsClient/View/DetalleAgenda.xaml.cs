﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserDetailsClient.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetalleAgenda : ContentPage
    {
        public DetalleAgenda()
        {
            InitializeComponent();

            if (MainPage.DNI != null)
            {
                lblTitulo.Text = AgendaProfesor.asunto.titulo;
                lblDescripcion.Text = AgendaProfesor.asunto.descripcion;
                lblEnviado.Text = AgendaProfesor.asunto.emisor;
                lblFecha.Text = AgendaProfesor.asunto.fecha.ToString();
                lblRecibido.Text = AgendaProfesor.asunto.receptor;
                lblAlumno.Text = AgendaProfesor.asunto.idAlumnoNavigation.idAlumno;
            }

            else if (MainPage.matricula != null)
            {
                lblTitulo.Text = AgendaPadres.asunto.titulo;
                lblDescripcion.Text = AgendaPadres.asunto.descripcion;
                lblEnviado.Text = AgendaPadres.asunto.emisor;
                lblFecha.Text = AgendaPadres.asunto.fecha.ToString();
                lblRecibido.Text = AgendaPadres.asunto.receptor;
                lblAlumno.Text = AgendaPadres.asunto.idAlumnoNavigation.idAlumno;
            }


        }
    }
}