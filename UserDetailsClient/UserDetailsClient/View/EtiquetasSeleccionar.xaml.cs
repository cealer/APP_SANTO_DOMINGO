﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UserDetailsClient.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserDetailsClient.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EtiquetasSeleccionar : ContentPage
    {
        public Etiquetas etiquetaSelect;
        public List<Etiquetas> ListaEtiquetas { get; set; }
        List<string> users = new List<string>();

        public static List<Etiquetas> ListaEtiqueta = new List<Etiquetas>();


        public EtiquetasSeleccionar()
        {
            InitializeComponent();

            ListaEtiquetas = new List<Models.Etiquetas> {
                new Models.Etiquetas { descripcion = "PELEA", estado = "1", idEtiqueta = "ET00001" },
                new Models.Etiquetas { descripcion = "SALIDA TEMPRANA", estado = "1", idEtiqueta = "ET00002" },
new Models.Etiquetas { descripcion = "RECORDATORIO DE PAGO", estado = "1", idEtiqueta = "ET00003" }

            };

            lvEtiquetas.ItemsSource = ListaEtiquetas;

            var personDataTemplate = new DataTemplate(() =>
            {
                var grid = new Grid();

                var nameLabel = new Label { FontAttributes = FontAttributes.Bold };
                var ageLabel = new Label();
                var btnAgregar = new Button { Text = "Agregar" };

                btnAgregar.Clicked += BtnAgregar_Clicked;

                nameLabel.SetBinding(Label.TextProperty, "descripcion");
                ageLabel.SetValue(Label.TextProperty, "descripcion");

                grid.Children.Add(nameLabel, 1, 0);
                grid.Children.Add(ageLabel);
                grid.Children.Add(btnAgregar, 2, 0);


                return new ViewCell { View = grid };
            });

            lvEtiquetas.ItemTemplate = personDataTemplate;

        }

        private void BtnAgregar_Clicked(object sender, EventArgs e)
        {
            if (etiquetaSelect != null)
            {
                ListaEtiqueta.Add(etiquetaSelect);
                DisplayAlert("Mensaje", $"Se agrego la etiqueta {etiquetaSelect.descripcion}", "OK");
            }
            else
            {
                DisplayAlert("Mensaje", "Debe seleccionar una etiqueta", "OK");
            }

        }

        private void sbBuscar_SearchButtonPressed(object sender, EventArgs e)
        {
            var search = sbBuscar.Text;
            lvEtiquetas.ItemsSource = new List<Etiquetas> { new Etiquetas { descripcion = "PELEA", estado = "1", idEtiqueta = "ET00005" } };
        }

        private void lvEtiquetas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            etiquetaSelect = (Etiquetas)e.SelectedItem;
        }

        private void btnEnviar_Clicked(object sender, EventArgs e)
        {
            
            foreach (var item in PadreFamilia.listaPadres)
            {
                users.Add(item.id);
                Models.AsuntoAgenda enviarAsunto = MainPage.asuntoAgenda;
                enviarAsunto.estado = "1";
                enviarAsunto.visto = false;
                enviarAsunto.idReceptor = item.id;
                enviarAsunto.receptor = $"{item.apellidos} {item.nombres}";
                enviarAsunto.fecha = DateTime.Now;

                //Guardar Notificaciones
            
            }

            foreach (var item in ListaEtiqueta)
            {
                //Guardar detalle asunto
            }

            //Enviar Notificacion

            EnviarNotificacion();

            DisplayAlert("MENSAJE", "ENVIADO", "OK");
            ListaEtiqueta = new List<Etiquetas>();
            View.PadreFamilia.listaPadres = new List<Models.PadreFamilia>();
        }

        public async void EnviarNotificacion()
        {
            var mensaje = new Mensaje { Descripcion = MainPage.asuntoAgenda.descripcion, Titulo = MainPage.asuntoAgenda.titulo, Users = users };
            var request = JsonConvert.SerializeObject(mensaje);
            var content = new StringContent(request, Encoding.UTF8, "application/json");
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://santodomingohub20171130044351.azurewebsites.net/");
            var url = string.Format("{0}/{1}", "api", "SendNotification");
            var response = await client.PostAsync(url, content);
        }
    }
}