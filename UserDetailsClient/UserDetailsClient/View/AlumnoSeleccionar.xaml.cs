﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserDetailsClient.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserDetailsClient.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlumnoSeleccionar : ContentPage
    {
        public static Alumno alumno;

        public AlumnoSeleccionar()
        {
            InitializeComponent();

            var ListaAlumnos = new List<Alumno> {
                new Models.Alumno { estado="1",idAlumno="AL000001",idGradosSecciones="GS000001",idPersona="PE000001",matricula="12345678",idPersonaNavigation=new IdPersonaNavigation{apellidoM="GONZALES",apellidoP="AQUIJE",nombres="CESAR",idPersona="PE000001" } },
                new Models.Alumno { estado="1",idAlumno="AL000002",idGradosSecciones="GS000001",idPersona="PE000002",matricula="12345678",idPersonaNavigation=new IdPersonaNavigation{apellidoM="GIRALDO",apellidoP="RODRIGUEZ",nombres="MIGUEL",idPersona="PE000002" } }
            };

            lvAlumnos.ItemsSource = ListaAlumnos;

            var personDataTemplate = new DataTemplate(() =>
            {
                var grid = new Grid();

                var ApePLabel = new Label { FontAttributes = FontAttributes.Bold };
                var ApeMLabel = new Label();
                var nombresLabel = new Label { HorizontalTextAlignment = TextAlignment.End };

                ApePLabel.SetBinding(Label.TextProperty, "idPersonaNavigation.apellidoP");
                ApeMLabel.SetBinding(Label.TextProperty, "idPersonaNavigation.apellidoM");
                nombresLabel.SetBinding(Label.TextProperty, "idPersonaNavigation.nombres");
                grid.Children.Add(ApePLabel, 1, 0);
                grid.Children.Add(ApeMLabel);
                grid.Children.Add(nombresLabel, 2, 0);

                return new ViewCell { View = grid };
            });

            lvAlumnos.ItemTemplate = personDataTemplate;

        }

        private void sbAlumno_SearchButtonPressed(object sender, EventArgs e)
        {
            var text = sbAlumno.Text;
            lvAlumnos.ItemsSource = new List<Alumno> { new Models.Alumno { estado = "1", idAlumno = "AL000001", idGradosSecciones = "GS000001", idPersona = "PE000001", matricula = "12345678", idPersonaNavigation = new IdPersonaNavigation { apellidoM = "GONZALES", apellidoP = "AQUIJE", nombres = "CESAR", idPersona = "PE000001" } } };
        }

        private void lvAlumnos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            alumno = (Alumno)e.SelectedItem;
            MainPage.asuntoAgenda.idAlumno = alumno.idAlumno;
            Navigation.PushAsync(new PadreFamilia());
        }
    }
}