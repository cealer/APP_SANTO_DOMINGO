﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserDetailsClient.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NuevoAsunto : ContentPage
    {

        public NuevoAsunto()
        {
            InitializeComponent();
        }

        private void btnSiguiente_Clicked(object sender, EventArgs e)
        {
            MainPage.asuntoAgenda.descripcion = edtDescripcion.Text;
            MainPage.asuntoAgenda.titulo = edtTitulo.Text;
            Navigation.PushAsync(new EtiquetasSeleccionar());
        }
    }
}