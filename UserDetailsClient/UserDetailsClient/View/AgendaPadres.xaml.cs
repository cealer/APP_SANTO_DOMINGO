﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserDetailsClient.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgendaPadres : ContentPage
    {
        public static Models.AsuntoAgenda asunto;

        public AgendaPadres()
        {
            InitializeComponent();

            var ListaAgenda = new List<Models.AsuntoAgenda> {
                new Models.AsuntoAgenda { estado="1",idAlumno="AL000001",descripcion="ASDASD",titulo="asdd",emisor="Profesor Luis",fecha=DateTime.Now}

            };

            lvAgendaPadres.ItemsSource = ListaAgenda;

            var personDataTemplate = new DataTemplate(() =>
            {
                var grid = new Grid();

                var ApePLabel = new Label { FontAttributes = FontAttributes.Bold };
                var ApeMLabel = new Label();
                var nombresLabel = new Label { HorizontalTextAlignment = TextAlignment.End };

                ApePLabel.SetBinding(Label.TextProperty, "titulo");
                ApeMLabel.SetBinding(Label.TextProperty, "emisor");
                nombresLabel.SetBinding(Label.TextProperty, "fecha");
                grid.Children.Add(ApePLabel, 1, 0);
                grid.Children.Add(ApeMLabel);
                grid.Children.Add(nombresLabel, 2, 0);

                return new ViewCell { View = grid };
            });

            lvAgendaPadres.ItemTemplate = personDataTemplate;


        }

        private void lvAgendaPadres_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            asunto = (Models.AsuntoAgenda)e.SelectedItem;
            Navigation.PushAsync(new DetalleAgenda());
        }
    }
}