﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UserDetailsClient.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PadreFamilia : ContentPage
    {

        public static List<Models.PadreFamilia> listaPadres = new List<Models.PadreFamilia>();

        public Models.PadreFamilia PadreActual { get; set; }

        public PadreFamilia()
        {
            InitializeComponent();

            var Padres = new List<Models.PadreFamilia>
            {
                new Models.PadreFamilia{id = "69901fe0-2e02-465b-a3ea-e530e51d199a", apellidos = "P. Osambela", nombres = "Milagros"},
                new Models.PadreFamilia{id = "f1fc72b9-29b4-4fb8-a31a-920f8513d500", apellidos = "Gonzales Aquije", nombres = "Cesar"}
            };


            lvPadre.ItemsSource = Padres;

            var personDataTemplate = new DataTemplate(() =>
            {
                var grid = new Grid();

                var ApePLabel = new Label { FontAttributes = FontAttributes.Bold };
                var ApeMLabel = new Label();
                var btnAgregar = new Button { Text = "Agregar" };

                ApePLabel.SetBinding(Label.TextProperty, "apellidos");
                ApeMLabel.SetBinding(Label.TextProperty, "nombres");

                btnAgregar.Clicked += BtnAgregar_Clicked;

                grid.Children.Add(ApePLabel, 1, 0);
                grid.Children.Add(ApeMLabel);
                grid.Children.Add(btnAgregar, 2, 0);

                return new ViewCell { View = grid };
            });

            lvPadre.ItemTemplate = personDataTemplate;


        }

        private void BtnAgregar_Clicked(object sender, EventArgs e)
        {
            if (PadreActual != null)
            {
                listaPadres.Add(PadreActual);
                DisplayAlert("Mensaje", $"Se agrego la etiqueta {PadreActual.apellidos} {PadreActual.nombres}", "OK");
            }
            else
            {
                DisplayAlert("Mensaje", "Debe seleccionar una etiqueta", "OK");
            }
        }

        private void btnSiguiente_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new NuevoAsunto());
        }

        private void lvPadre_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            PadreActual = (Models.PadreFamilia)e.SelectedItem;
        }
    }
}