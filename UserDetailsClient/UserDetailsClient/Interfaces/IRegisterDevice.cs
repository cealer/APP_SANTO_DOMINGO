﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDetailsClient.Interfaces
{
    public interface IRegisterDevice
    {
        void RegisterDevice();
    }

}
