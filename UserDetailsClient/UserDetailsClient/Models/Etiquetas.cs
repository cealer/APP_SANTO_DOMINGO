﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDetailsClient.Models
{
    public class Etiquetas
    {
        public string idEtiqueta { get; set; }
        public string descripcion { get; set; }
        public string estado { get; set; }
    }
}
