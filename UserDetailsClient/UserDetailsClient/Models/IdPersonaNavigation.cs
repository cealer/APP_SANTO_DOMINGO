﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDetailsClient.Models
{
    public class IdPersonaNavigation
    {
        public string idPersona { get; set; }
        public string nombres { get; set; }
        public string apellidoP { get; set; }
        public string apellidoM { get; set; }
        public string dni { get; set; }
        public string sexo { get; set; }
        public object direccion { get; set; }
        public object numContacto { get; set; }
        public string estado { get; set; }
        public object idTipoPersona { get; set; }
        public DateTime fecNac { get; set; }
        public string datos { get; set; }
        public object idTipoPersonaNavigation { get; set; }



    }
}
