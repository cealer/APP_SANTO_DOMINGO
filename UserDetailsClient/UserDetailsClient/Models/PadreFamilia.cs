﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDetailsClient.Models
{
    public class PadreFamilia
    {
        public string id { get; set; }
        public string apellidos { get; set; }
        public string nombres { get; set; }

    }
}
