﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDetailsClient.Models
{
    public class Alumno
    {
        public string idAlumno { get; set; }
        public string idGradosSecciones { get; set; }
        public string idPersona { get; set; }
        public string matricula { get; set; }
        public string estado { get; set; }
        public IdPersonaNavigation idPersonaNavigation { get; set; }
    }
}
