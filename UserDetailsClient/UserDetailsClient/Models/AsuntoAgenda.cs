﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDetailsClient.Models
{
    public class AsuntoAgenda
    {
        public string idAsuntoAgenda { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        public string idReceptor { get; set; }
        public string idEmisor { get; set; }
        public string emisor { get; set; }
        public string receptor { get; set; }
        public bool visto { get; set; }
        public DateTime fecha { get; set; }
        public string idAlumno { get; set; }
        public string estado { get; set; }
        public Alumno idAlumnoNavigation { get; set; }
    }
}