﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDetailsClient.Models
{
   public class Mensaje
    {
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public List<string> Users { get; set; }
    }
}
